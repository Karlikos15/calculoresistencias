public class Resistencia {

    private Double valor;
    private Integer nodo1;
    private Integer nodo2;
    
    public Resistencia(Double valor, Integer nodo1, Integer nodo2) {
        this.valor = valor;
        this.nodo1 = nodo1;
        this.nodo2 = nodo2;
    }
    
    public Resistencia() {
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Double getValor() {
        return valor;
    }

    public void setNodo1(Integer nodo1) {
        this.nodo1 = nodo1;
    }

    public Integer getNodo1() {
        return nodo1;
    }

    public void setNodo2(Integer nodo2) {
        this.nodo2 = nodo2;
    }

    public Integer getNodo2() {
        return nodo2;
    }
}
