import java.awt.Color;

//Eventos del botón
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.math.BigDecimal;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTextField;

//Eventos del Spinner
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class Main extends JFrame{
    private int nodos = 4;
    private Resistencia[] resistencias;
    private int loop = 0;
    
    //Elementos visuales
    private JTextField[] txtResistencias;
    private JRadioButton[] rdNodos;
    private JSpinner spNodos = new JSpinner();//Selecciona el número de nodos
    private JButton btnCalcular = new JButton("Calcular Resistencia e Intensidad");
    private JLabel lblResultado = new JLabel();
    private JLabel lblNodos = new JLabel("Nº de nodos: ");
    private JTextField pila = new JTextField("0");
    
    //Variable de control del evento para seleccionar el nº de nodos
    private boolean sw = true;
    
    /**
     * Constructor de la clase principal. Iniciamos los elementos fijos de la pantalla.
     */
    public Main() {
        this.setBounds(0, 0, 800, 600);
        this.setTitle("Circuitos eléctricos con asociaciones mixtas de resistencias");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//Metodo para finalizar el programa al cerrar la ventana
        this.getContentPane().setLayout(null);//No definimos un orden especifico de ningun elemento dibujado sobre la ventana donde trabajemos
        lblNodos.setBounds(0, 30, 80, 25);
        spNodos.setBounds(80, 30, 40, 25);
        spNodos.setValue(nodos);
        
        //Evento para controlar el nº de nodos
        spNodos.addChangeListener(new ChangeListener(){
                public void stateChanged(ChangeEvent e) {
                    if(sw){
                        sw = false;
                        int valor = Integer.parseInt(spNodos.getNextValue().toString());
                        
                        if(valor>nodos){
                            nodos=nodos+2;
                        }else{
                            nodos=nodos-2;
                        }
                        if(nodos<4){
                            nodos = 4;
                        }
                        spNodos.setValue(nodos);
                        dibujarNodos();
                    }else{
                        sw = true;
                    }
                }
            });
        
        btnCalcular.setBounds(30, 150, 250, 25);
        
        //Evento para calcular la Resistencia total y la Intensidad
        btnCalcular.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                calcularResistencia();
            }
        });

        pila.setBounds(21, 76, 34, 34);
        pila.setHorizontalAlignment(JTextField.CENTER);
        pila.setBackground(Color.BLACK);
        pila.setForeground(Color.WHITE);
        lblResultado.setBounds(300, 150, 400, 20);
        
        this.dibujarNodos();
        this.setVisible(true);
    }
    
    /**
     * Metodo que coloca todos los elementos visuales generando las resistencias y los nodos correspondientes
     */
    public void dibujarNodos(){
        //colocamos los elementos fijos
        this.getContentPane().removeAll();
        this.getContentPane().add(lblNodos);
        this.getContentPane().add(spNodos);
        this.getContentPane().add(pila);
        this.getContentPane().add(btnCalcular);
        this.getContentPane().add(lblResultado);
        
        //Inicializamos los arrays de nodos y resistencias
        rdNodos = new JRadioButton[nodos];
        txtResistencias = new JTextField[((nodos/2)-1)*3];
        
        JRadioButton rdNodo;
        JTextField txt;
        
        //generamos los nodos y las resistencias
        for(int n=0, r=0;n<nodos;n++){//Mientras n(nº posicion del nodo) sea menor que el numero de nodos, con cada bucle, se sume un nodo mas
            rdNodo = new JRadioButton();//Generamos un nuevo nodo(circulo visual)
            rdNodo.setSelected(true);//Marcar el nodos como activo
            rdNodo.setEnabled(false);//No se permite que se pueda modificar el nodo
            txt = new JTextField("0");
            txt.setHorizontalAlignment(JTextField.CENTER);
            txt.setBackground(Color.CYAN);
            if(n<(nodos/2)){
                //Dibujamos la primera mitad de nodos
                rdNodo.setBounds(30+n*50, 60, 16, 16);
                if(n>0){
                    //Dibujamos resistencias en serie de la primera mitad
                    txt.setBounds(50+(n-1)*50, 60, 30, 16);
                }
            }else{
                //Dibujamos la segunda mitad de nodos
                rdNodo.setBounds(30+(nodos-n-1)*50, 110, 16, 16);
                if(n<nodos-1){
                    //Dibujamos resistencias en serie de la segunda mitad
                    txt.setBounds(50+(nodos-n-2)*50, 110, 30, 16);
                }
            }
            rdNodos[n] = rdNodo;
            this.getContentPane().add(rdNodo);
            if(n>0 && n<nodos-1){
                //Colocamos las resistencias en serie
                txtResistencias[r++]=txt;
                this.getContentPane().add(txt);
                if(n<nodos/2){
                    //Si estamos en la primera mitad de nodos, dibujamos y colocamos también las resistencias en paralelo
                    txt = new JTextField("0");
                    txt.setHorizontalAlignment(JTextField.CENTER);
                    txt.setBackground(Color.CYAN);
                    txt.setBounds(75+(n-1)*50, 85, 30, 16);
                    txtResistencias[r++]=txt;
                    this.getContentPane().add(txt);
                }
            }
        }
        
        //refrescamos la pantalla para que se apliquen los cambios
        this.repaint();
    }
    
    /**
     * Metodo para recoger las resistencias válidas, e iniciar el cálculo de la resistencia total
     */
    public void calcularResistencia(){
        loop = 0;
        
        //Inicializamos el array de resistencias
        resistencias = new Resistencia[((nodos/2)-1)*3];
        
        Resistencia res;
        System.out.println("Resistencias-->" + txtResistencias.length);
        System.out.println("Nodos-->" + nodos);
        for(int i=0; i<txtResistencias.length; i++){
            if(!txtResistencias[i].getText().trim().equals("0") && !txtResistencias[i].getText().trim().equals("")){
                //Si el valor de la resistencia es distinto de 0 o de vacio, recojo su valor y lo añado al array de resistencias
                res = new Resistencia();
                try{
                    if(i<=(txtResistencias.length-(nodos/2))){
                        if(i%2==0){
                            //Resistencias en serie de la fila de arriba
                            res.setNodo1(i/2+1);
                            res.setNodo2(res.getNodo1()+1);
                            res.setValor(new Double(txtResistencias[i].getText().trim()));
                        }else if(i%2==1){
                            //Resistencias en paralelo
                            res.setNodo1((i+3)/2);
                            res.setNodo2(nodos-(i+1)/2);
                            res.setValor(new Double(txtResistencias[i].getText().trim()));
                        }
                    }else{
                        //Resistencias en serie de la fila de abajo.
                        res.setNodo1(i-(txtResistencias.length-nodos));
                        res.setNodo2(res.getNodo1()+1);
                        res.setValor(new Double(txtResistencias[i].getText().trim()));
                    }
                    System.out.println(i + "-->nodo(" + res.getNodo1() + ")(" + res.getNodo2() + ")-->" + res.getValor());
                    resistencias[i] = res;
                }catch(NumberFormatException nfe){
                    //Si el valor no es valido, borramos la resistencia de la vista.
                    this.getContentPane().remove(txtResistencias[i]);
                    resistencias[i] = null;
                }
            }else{
                //Si el valor es 0 o vacio, borramos la resistencia de la vista.
                this.getContentPane().remove(txtResistencias[i]);
                resistencias[i] = null;
            }
        }
        
        //Iniciamos el calculo con las resistencias en serie.
        BigDecimal db = new BigDecimal(calcularSerie((nodos/2)-1, new Double(0)));
        try{
            if(new Double (pila.getText().trim().replace(",", "."))>=0){
                lblResultado.setText("La Resistencia total es: " + db.setScale(2, BigDecimal.ROUND_HALF_UP) + ". La Intensidad es: " + new BigDecimal(new Double (pila.getText().trim().replace(",", "."))/db.doubleValue()).setScale(4, BigDecimal.ROUND_HALF_UP));
            }else{
                lblResultado.setText("El valor de la pila debe ser mayor o igual que 0");
            }
        }catch(NumberFormatException nfe){
            lblResultado.setText("El valor de la pila debe ser numérico");
        }
        this.repaint();
    }
    
    /**
     * Metodo recursivo para calcular las resistencias en serie. En cada llamada se va actualizando el valor
     * del nodo inicial y del valor inicial.
     * 
     * param nodo Es el nodo a partir del cual sumaremos 3 resistencias en serie.
     * param valor Es el valor inicial para el calculo.
     * 
     * return Double Devuelve el valor de la resistencia calculada.
     */
    private Double calcularSerie(Integer nodo, Double valor){
        if(nodo==0){
            //Si el nodo es 0 se termina de realizar los cálculos y devolvemos el valor final
            return valor;
        }
        System.out.println("Calculando en Serie nodos:");
        System.out.println("(" + nodo + ")(" + (nodo+1) + ")");
        System.out.println("(" + (nodo+1) + ")(" + (nodo+2+(loop>0?loop:0)) + ")");
        System.out.println("(" + (nodo+2+(loop>0?loop:0)) + ")(" + (nodo+3+(loop>0?loop:0)) + ")");
        Integer resParalelo = null;
        for(int i=0;i<resistencias.length;i++){
            Resistencia res = resistencias[i];
            if(res!=null){
                //Si la resistencia es válida comprobamos si está en serie
                if( (res.getNodo1().equals(nodo) && res.getNodo2().equals(nodo+1)) ||
                    (res.getNodo1().equals(nodo+1) && res.getNodo2().equals(nodo+2+(loop>0?loop:0))) ||
                    (res.getNodo1().equals(nodo+2+(loop>0?loop:0)) && res.getNodo2().equals(nodo+3+(loop>0?loop:0)))){
                    System.out.println("sum " + res.getValor());
                    valor += res.getValor();
                    resistencias[i] = null;
                }
                System.out.println("par1-->(" + res.getNodo1() + ")(" + res.getNodo2() + ")");
                System.out.println("par2-->(" + nodo + ")(" + (nodo+3+(loop>0?loop:0)) + ")");
                if(res.getNodo1().equals(nodo) && res.getNodo2().equals(nodo+3+(loop>0?loop:0))){
                    //Si existe resistencia en paralelo, guardamos su numero de posición
                    resParalelo = i;
                }
            }
        }
        loop=loop+2;
        
        if(valor>0 && resParalelo!=null){
            //Si tenemos resistencia en paralelo y un valor de resistencias en serie, calculamos el valor en equivalente entre ambas
            double db = calcularParalelo(valor, resistencias[resParalelo].getValor());
            resistencias[resParalelo] = null;
            return calcularSerie(nodo-1, db);
        }else{
            //Si no tenemos resistencia en paraleo, o el valor en serie es 0, calculamos la siguiente tanda(pasada) en serie
            return calcularSerie(nodo-1, valor);
        }
    }
    
    /**
     * Metodo para calcular las resistencias en paralelo.
     * 
     * param valor1 Primer valor para el calculo en paralelo.
     * param valor2 Segundo valor para el calculo en paralelo.
     * 
     * return Double Devuelve el valor de la resistencia calculada.
     */
    private Double calcularParalelo(Double valor1, Double valor2){
        System.out.println("Calculando en Paralelo para valores " + valor1 + " y " + valor2);
        return 1d/((1d/valor1)+(1d/valor2));
    }
    
    /**
     * Metodo inicial de ejecución
     */
    public static void main(String[] args) {
        new Main();
    }
    
}